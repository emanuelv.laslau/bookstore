var initMenu = function () {
    var menu = $('#menu');

    $('.dropdown-toggle', menu).on('click', function (event) {
        event.preventDefault();

        var target = $(this);
        var list = target.next();

        list.toggleClass('show');
    });
};

$(document).ready(function () {
    initMenu();
});