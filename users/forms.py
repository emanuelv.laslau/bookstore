from django import forms

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm, UserCreationForm

from .models import UserRegistration

county = (
    ("0", "Județul de expediție",),
    ("Alba", "Alba",),
    ("Arad", "Arad",),
    ("Arges", "Argeș",),
    ("Bacau", "Bacău",),
    ("Bihor", "Bihor",),
    ("Bistrita-Nasaud", "Bistrița-Năsăud",),
    ("Botosani", "Botoșani",),
    ("Braila", "Brăila",),
    ("Brasov", "Brașov",),
    ("Bucuresti", "București",),
    ("Buzau", "Buzău",),
    ("Calarasi", "Călărași",),
    ("Caras-Severin", "Caraș-Severin",),
    ("Cluj", "Cluj",),
    ("Constanta", "Constanța",),
    ("Covasna", "Covasna",),
    ("Dambovita", "Dâmbovița",),
    ("Dolj", "Dolj",),
    ("Galati", "Galați",),
    ("Giurgiu", "Giurgiu",),
    ("Gorj", "Gorj",),
    ("Harghita", "Harghita",),
    ("Hunedoara", "Hunedoara",),
    ("Ialomita", "Ialomița",),
    ("Iasi", "Iași",),
    ("Ilfov", "Ilfov",),
    ("Maramures", "Maramureș",),
    ("Mehedinti", "Mehedinți",),
    ("Mures", "Mureș",),
    ("Neamt", "Neamț",),
    ("Olt", "Olt",),
    ("Prahova", "Prahova",),
    ("Salaj", "Sălaj",),
    ("Satu Mare", "Satu Mare",),
    ("Sibiu", "Sibiu",),
    ("Suceava", "Suceava",),
    ("Teleorman", "Teleorman",),
    ("Timis", "Timiș",),
    ("Tulcea", "Tulcea",),
    ("Valcea", "Vâlcea",),
    ("Vaslui", "Vaslui",),
    ("Vrancea", "Vrancea",),
)
country = (
    ("Romania", "Romania",),
)


class UserSignupForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email_confirmation = forms.EmailField()
    country = forms.ChoiceField(choices=country)
    county = forms.ChoiceField(choices=county)
    email_communication = forms.BooleanField(required=False)
    mail_communication = forms.BooleanField(required=False)
    phone_communication = forms.BooleanField(required=False)

    class Meta(UserCreationForm):
        model = UserRegistration
        fields = (
            'first_name',
            'last_name',
            'email',
            'email_confirmation',
            'phone_no',
            'country',
            'county',
            'city',
            'street',
            'zip_code',
            'thumbnail',
            'email_communication',
            'mail_communication',
            'phone_communication',
            'password1',
            'password2'
        )

    def clean(self):
        cleaned_data = super(UserSignupForm, self).clean()
        email = cleaned_data.get('email')
        email_confirmation = cleaned_data.get('email_confirmation')

        if email and email_confirmation and email != email_confirmation:
            self._errors['email_confirmation'] = self.error_class(['Emails do not match.'])
            del self.cleaned_data['email_confirmation']
        return cleaned_data


class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', required=True)
    last_name = forms.CharField(label='Last Name', required=True)
    email_communication = forms.BooleanField(required=False)
    mail_communication = forms.BooleanField(required=False)
    phone_communication = forms.BooleanField(required=False)

    class Meta:
        model = UserRegistration
        fields = (
            'first_name',
            'last_name',
            'phone_no',
            'email',
            'country',
            'county',
            'city',
            'street',
            'zip_code',
            'thumbnail',
            'email_communication',
            'mail_communication',
            'phone_communication'
        )


class EmailChangeForm(forms.ModelForm):
    email_confirmation = forms.EmailField()

    class Meta:
        model = UserRegistration
        fields = (
            'email',
            'email_confirmation'
        )

    def clean(self):
        cleaned_data = super(EmailChangeForm, self).clean()
        email = cleaned_data.get('email')
        email_confirmation = cleaned_data.get('email_confirmation')

        if email and email_confirmation and email != email_confirmation:
            self._errors['email_confirmation'] = self.error_class(['Emails do not match.'])
            del self.cleaned_data['email_confirmation']
        return cleaned_data


class EmailValidationOnForgotPassword(PasswordResetForm):

    def clean_email(self):
        email = self.cleaned_data['email']
        if not UserRegistration.objects.filter(email__iexact=email, is_active=True).exists():
            msg = _("There is no user registered with the specified E-Mail address.")
            self.add_error('email', msg)
        return email
