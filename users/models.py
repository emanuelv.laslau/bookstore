"""Declare models for YOUR_APP app."""

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import get_thumbnail

from .managers import UserManager


class UserRegistration(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    phone_no = models.CharField(max_length=13)
    country = models.CharField(max_length=35, default='Romania')
    county = models.CharField(max_length=35)
    city = models.CharField(max_length=35)
    address = models.CharField(max_length=225)
    street = models.CharField(max_length=105)
    zip_code = models.IntegerField(default=000000)
    thumbnail = models.ImageField(upload_to='user/thumbnail/%Y/%m/%d/',
                                  blank=True, default='no_profile_img.png', help_text='Optional')
    email_communication = models.BooleanField(default=False)
    mail_communication = models.BooleanField(default=False)
    phone_communication = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    @property
    def thumbnail_preview(self):
        if self.thumbnail:
            _thumbnail = get_thumbnail(self.thumbnail,
                                       '150x150',
                                       upscale=False,
                                       crop=False,
                                       quality=100)
            return format_html(
                '<img src="{}" width="{}" height="{}">'.format(_thumbnail.url, _thumbnail.width, _thumbnail.height))
        return ""

    @property
    def avatarURL(self):
        try:
            url = self.thumbnail.url
        except:
            url = ''
        return url

    objects = UserManager()

    def __str__(self):
        return "{}".format(self.email)


class RejectedUserAccountRequest(models.Model):
    email = models.EmailField(_('email address'))
    rejected_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.email


class RequestMade(models.Model):
    ip = models.CharField(max_length=65)
    date_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.ip
