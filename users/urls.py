from django.contrib.auth.views import PasswordChangeView
from django.urls import path

from .views import (
    ProfileView,
    UsersLoginView,
    ProfileUpdateView,
    UsersLogoutView,
    SignUpView,
    UserPasswordChangeView,
    UserEmailChangeView,
    UserPasswordResetConfirmView,
    UserPasswordResetDoneView,
    UserPasswordResetFormView,
    UserResetPasswordComplete,
)

app_name = 'users'

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('login/', UsersLoginView.as_view(), name='login'),
    path('login/<int:error_id>', UsersLoginView.as_view(), name='login-advance'),
    path('logout/', UsersLogoutView.as_view(), name='logout'),
    path('change-password', UserPasswordChangeView.as_view(), name='change_password'),
    path('change-email', UserEmailChangeView.as_view(), name='change_email'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('profile-update/', ProfileUpdateView.as_view(), name='profile_update'),
    path('password-reset/', UserPasswordResetFormView.as_view(), name='password_reset'),
    path('password-reset/done/', UserPasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password-reset/complete/', UserResetPasswordComplete.as_view(), name='password_reset_complete'),
    path('reset/<uidb64>/<token>/', UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),

]
