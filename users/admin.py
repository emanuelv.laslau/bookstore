from django.contrib import admin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import RejectedUserAccountRequest, RequestMade, UserRegistration


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm

    fieldsets = (
        (None, {'fields': ('email', 'password',)}),

        (_('Personal info'),
         {'fields': (
             'first_name', 'last_name',
             'phone_no', 'country', 'city', 'address',
             'street', 'zip_code', 'thumbnail',
             'email_communication', 'mail_communication',
             'phone_communication')}),

        (_('Permissions'),
         {'fields': ('is_active', 'is_staff', 'is_superuser',
                     'groups', 'user_permissions')}),

        (_('Important dates'),
         {'fields': ('last_login', 'date_joined')}),

    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ['email', 'id', 'first_name', 'last_name', 'is_staff', "phone_no"]
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('id',)

    readonly_fields = ('thumbnail_preview',)

    def thumbnail_preview(self, obj):
        return obj.thumbnail_preview

    thumbnail_preview.short_description = 'Thumbnail Preview'
    thumbnail_preview.allow_tags = True


admin.site.register(UserRegistration, UserAdmin)
admin.site.register(RequestMade)
admin.site.register(RejectedUserAccountRequest)
