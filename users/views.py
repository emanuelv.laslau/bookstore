import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import *

from django.urls import reverse_lazy
from django.utils import timezone

from django.views.generic import CreateView, TemplateView, UpdateView

from .forms import EmailChangeForm, EmailValidationOnForgotPassword, UserSignupForm, UserUpdateForm
from .models import UserRegistration, RequestMade


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'user/profile/profile_view.html'


class ProfileUpdateView(UpdateView, LoginRequiredMixin):
    model = UserRegistration
    form_class = UserUpdateForm
    template_name = 'user/profile/profile_update.html'
    success_url = reverse_lazy('users:profile')

    def get_object(self, queryset=None):
        return self.request.user


class UserEmailChangeView(UpdateView, LoginRequiredMixin):
    model = UserRegistration
    form_class = EmailChangeForm
    template_name = 'user/profile/change_email.html'
    success_url = reverse_lazy('users:profile')

    def get_object(self, queryset=None):
        return self.request.user


class SignUpView(CreateView):
    form_class = UserSignupForm
    success_url = reverse_lazy('users:login')
    template_name = 'user/profile/signup.html'


def get_client_ip(request):
    remote_address = request.META.get('HTTP_X_FORWARDED_FOR') or request.META.get('REMOTE_ADDR')
    ip = remote_address

    return ip


class UsersLoginView(LoginView):
    template_name = 'user/login.html'

    def post(self, request, *args, **kwargs):

        # cap_url = 'https://www.google.com/recaptcha/api.js'
        #
        # captcha_token = request.POST.get('g-recaptcha-response')
        # cap_secret = '6LddvEMaAAAAABQ95vLQDDcseXHObAK_WMOavBmt'  # V 3
        # cap_data = {
        #     'secret': cap_secret,
        #     'response': captcha_token
        # }
        # cap_server_response = request.post(url=cap_url, data=cap_data)
        # response_dict: dict = json.loads(cap_server_response.content)
        # print('RECAPTCHA:', response_dict)
        # if response_dict.get('success', False) and response_dict.get('score') > 0.5:
        #     return super().post(request, *args, **kwargs)
        # else:
        #     return self.render_to_response(self.get_context_data(error_id=1))

        ip = get_client_ip(request)
        request_made = RequestMade.objects.create(ip=ip)
        request_made.save()
        one_min_ago = timezone.now() - datetime.timedelta(minutes=1)
        previous_request = RequestMade.objects.filter(ip=ip, date_time__gt=one_min_ago).count()

        if previous_request > 3:
            return self.render_to_response(self.get_context_data(error_id=1))
        else:
            return super().post(request, *args, **kwargs)


class UsersLogoutView(LogoutView):
    template_name = 'user/logout.html'


class UserPasswordChangeView(PasswordChangeView, LoginRequiredMixin):
    template_name = 'user/password/change_password.html'
    success_url = '/'


class UserPasswordResetFormView(PasswordResetView):
    form_class = EmailValidationOnForgotPassword
    template_name = 'user/password/password_reset_form.html'
    success_url = reverse_lazy('users:password_reset_done')


class UserPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'user/password/password_reset_done.html'


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    success_url = reverse_lazy('users:password_reset_complete')
    template_name = 'user/password/password_reset_confirm.html'


class UserResetPasswordComplete(PasswordResetCompleteView):
    template_name = 'user/password/password_reset_complete.html'
