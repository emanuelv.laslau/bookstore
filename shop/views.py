from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.generic import DetailView, ListView

from .forms import ContactEmailForm
from .models import *


class Home(ListView):
    template_name = 'shop/home_page.html'
    context_object_name = 'slide_pics'

    def get_queryset(self):
        return SlideImg.objects.all()


def product_list_view(request, category_slug):
    return render(request, 'shop/product_list.html',
                  context={
                      'products': Product.objects.filter(category__slug=category_slug)
                  }
                  )


class ProductDetailView(DetailView):
    model = Product
    template_name = 'shop/product_details.html'


def about_us_view(request):
    return render(request, 'shop/about_us.html',
                  context={
                      'about': AboutUs.objects.all()
                  })


def contact(request):
    global subject, message, email
    if request.method == 'POST':
        form = ContactEmailForm(request.POST or None)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            email = form.cleaned_data['email']
            body = {
                'first_name': form.cleaned_data['first_name'],
                'last_name': form.cleaned_data['last_name'],
                'message': form.cleaned_data['message'],
            }
            message = "\n".join(body.values())

        try:
            send_mail(
                subject,
                message,
                email,
                ['laslau.emanuel@gmail.com'],
                fail_silently=False
                )

        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return redirect("shop:home")

    form = ContactEmailForm()
    return render(request, "shop/contact_us.html", {'form': form})
