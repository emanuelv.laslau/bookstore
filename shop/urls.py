from django.urls import path
from django.views.generic import TemplateView

from .views import Home, ProductDetailView, about_us_view, contact, product_list_view

app_name = 'shop'


urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('category/<str:category_slug>/', product_list_view, name='product_list'),
    path('product/<str:slug>/', ProductDetailView.as_view(), name='product_detail'),
    path('testimonials/', TemplateView.as_view(template_name='shop/testimonials.html'), name='testimonials'),
    path('contact-us/', contact, name='contact_us'),
    path('about/', about_us_view, name='about'),
    path('terms/', TemplateView.as_view(template_name='shop/terms.html'), name='terms'),

]
