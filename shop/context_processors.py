from django.shortcuts import render

from orders.models import Order
from .models import (
    Category,
    Contact,
    Product,
    Terms,
    TermsImg,
    Testimonials,
    TestimonialsImg,
)


def categories(request):
    return {
        'categories': Category.objects.filter(parent=None)
    }


def subcategories(request):
    return {
        'subcategories': Category.objects.filter(parent=True)
    }


def featured_products(request):
    return {
        'featured_products': Product.objects.filter(featured_product=True)
    }


def contact(request):
    return {
        'contact': Contact.objects.all()
    }


def testimonials(request):
    return {
        'testimonials': Testimonials.objects.all(),
        'image': TestimonialsImg.objects.all()
    }


def terms(request):
    return {
        'terms': Terms.objects.all(),
        'image': TermsImg.objects.all()
    }


def cart(request):
    if request.user.is_authenticated:
        customer = request.user.id
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        cartItems = order.get_cart_items

    else:
        order = {'get_cart_total': 0, 'get_cart_items': 0}
        cartItems = order['get_cart_items']

    return {
        'cartItems': cartItems
    }
