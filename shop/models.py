from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify
from sorl.thumbnail import get_thumbnail
from django.utils.html import format_html


# Create your models here.


class Testimonials(models.Model):
    title = models.CharField(max_length=75, blank=True, null=True)
    testimonial = models.TextField(blank=True, null=True, max_length=2_500)
    new_testimonial = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'testimonials'

    def __str__(self):
        return self.title


class TestimonialsImg(models.Model):
    image = models.ImageField(
        blank=True, null=True,
        upload_to='testimonials',
        default='blog-fullscreen-1-1920x700.jpg'
    )

    @property
    def thumbnail_preview(self):
        if self.image:
            _thumbnail = get_thumbnail(self.image,
                                       '150x150',
                                       upscale=False,
                                       crop=False,
                                       quality=100)
            return format_html(
                '<img src="{}" width="{}" height="{}">'.format(_thumbnail.url, _thumbnail.width, _thumbnail.height))
        return ""

    @property
    def testimonial_imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


class Terms(models.Model):
    title = models.CharField(max_length=75, blank=True, null=True)
    terms = models.TextField(blank=True, null=True, max_length=2_500)

    class Meta:
        verbose_name_plural = 'terms'

    def __str__(self):
        return self.title


class TermsImg(models.Model):
    image = models.ImageField(blank=True, null=True, upload_to='terms', default='banner-image-1-1920x500.jpg')

    @property
    def thumbnail_preview(self):
        if self.image:
            _thumbnail = get_thumbnail(self.image,
                                       '150x150',
                                       upscale=False,
                                       crop=False,
                                       quality=100)
            return format_html(
                '<img src="{}" width="{}" height="{}">'.format(_thumbnail.url, _thumbnail.width, _thumbnail.height))
        return ""

    @property
    def terms_imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


slide_img = (
    'slider-image-1-1920x700.jpg', 'slider-image-2-1920x700.jpg', 'slider-image-3-1920x700.jpg'
)


class SlideImg(models.Model):
    image = models.ImageField(upload_to='slider/%Y/%m/%d', default=slide_img)  # 'slider-image-1-1920x700.jpg')

    @property
    def thumbnail_preview(self):
        if self.image:
            _thumbnail = get_thumbnail(self.image,
                                       '150x150',
                                       upscale=False,
                                       crop=False,
                                       quality=100)
            return format_html(
                '<img src="{}" width="{}" height="{}">'.format(_thumbnail.url, _thumbnail.width, _thumbnail.height))
        return ""

    @property
    def slide_imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


class Contact(models.Model):
    map = models.ImageField(upload_to='map', default='map.jpg')
    email = models.EmailField()
    phone_no = models.CharField(max_length=10)
    address = models.CharField(max_length=350)

    class Meta:
        verbose_name_plural = 'contact'

    @property
    def map_imageURL(self):
        try:
            url = self.map.url
        except:
            url = ''
        return url

    def __str__(self):
        return self.email


class AboutUs(models.Model):
    image = models.ImageField(blank=True, null=True,
                              upload_to='image_about_us', default='banner-image-1-1920x500.jpg')

    description = models.TextField(blank=True, max_length=5_000)
    description_01 = models.TextField(blank=True, max_length=5_000)
    description_02 = models.TextField(blank=True, max_length=5_000)
    description_03 = models.TextField(blank=True, max_length=5_000)

    class Meta:
        verbose_name_plural = 'about us'

    @property
    def about_imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url

    def __str__(self):
        return self.description


class Author(models.Model):
    name = models.CharField(max_length=35)
    surname = models.CharField(max_length=35)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return f'{self.name} {self.surname}'


class Category(models.Model):
    title = models.CharField(max_length=250, unique=True)
    slug = models.SlugField(max_length=250, unique=True)
    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        related_name='child',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('title',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'
        unique_together = ('parent', 'title', 'slug')

    def get_absolut_url(self):
        return reverse('shop:product_list', kwargs={
            'pk': self.id,
            'category_slug': self.slug
        })

    @property
    def get_products(self):
        return Product.objects.filter(categories__name=self.title)

    def __str__(self):
        full_path = [self.title]
        k = self.parent
        while k is not None:
            full_path.append(k.title)
            k = k.parent
        return ' -> '.join(full_path[::-1])

    def save(self, *args, **kwargs):  # new
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    featured_product = models.BooleanField(null=True, blank=True, default=False)
    digital = models.BooleanField(default=False, null=True, blank=False)
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True, default=' ')
    thumbnail = models.ImageField(upload_to='product/thumbnail/%Y/%m/%d/',
                                  blank=False, default='no_cover.png')
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    discount_price = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    stock = models.IntegerField(default=0)

    def has_quantity(self):
        return self.stock > 0

    @property
    def thumbnail_preview(self):
        if self.thumbnail:
            _thumbnail = get_thumbnail(self.thumbnail,
                                       '150x150',
                                       upscale=False,
                                       crop=False,
                                       quality=100)
            return format_html(
                '<img src="{}" width="{}" height="{}">'.format(_thumbnail.url, _thumbnail.width, _thumbnail.height))
        return ""

    @property
    def product_imageURL(self):
        try:
            url = self.thumbnail.url
        except:
            url = ''
        return url

    class Meta:
        ordering = ('title',)
        verbose_name = 'product'
        verbose_name_plural = 'products'

    def __str__(self):
        return self.title

    def get_absolut_url(self):
        return reverse('shop:product_detail', kwargs={
            'pk': self.id,
            'product_slug': self.slug
        })

    def save(self, *args, **kwargs):  # new
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)
