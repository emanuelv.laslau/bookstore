from django.contrib import admin

# Register your models here.
from .models import (
    AboutUs,
    Category,
    Product,
    SlideImg,
    Author,
    Contact,
    Terms, TermsImg,
    Testimonials, TestimonialsImg
)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug')
    prepopulated_fields = {
        'slug': ('title',)
    }
    search_fields = ('title',)


class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'thumbnail_preview',
        'stock', 'category',
        'title',
        'featured_product',
        'price',
        'discount_price',
        'author'
    )

    prepopulated_fields = {
        'slug': ('title',)
    }
    readonly_fields = ('thumbnail_preview',)
    search_fields = ('title', 'category', 'author')

    def thumbnail_preview(self, obj):
        return obj.thumbnail_preview

    thumbnail_preview.short_description = 'Thumbnail Preview'
    thumbnail_preview.allow_tags = True


class ProductInlineAdmin(admin.TabularInline):
    model = Product


class AuthorAdmin(admin.ModelAdmin):
    inlines = (ProductInlineAdmin,)


class ContactAdmin(admin.ModelAdmin):
    list_display = ('email', 'phone_no', 'address',)


class TestimonialsImgAdmin(admin.ModelAdmin):
    list_display = (
        'thumbnail_preview',
    )

    readonly_fields = ('thumbnail_preview',)

    def thumbnail_preview(self, obj):
        return obj.thumbnail_preview

    thumbnail_preview.short_description = 'Thumbnail Preview'
    thumbnail_preview.allow_tags = True


class TermsImgAdmin(admin.ModelAdmin):
    list_display = (
        'thumbnail_preview',
    )

    readonly_fields = ('thumbnail_preview',)

    def thumbnail_preview(self, obj):
        return obj.thumbnail_preview

    thumbnail_preview.short_description = 'Thumbnail Preview'
    thumbnail_preview.allow_tags = True


admin.site.register(AboutUs)
admin.site.register(SlideImg)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Terms)
admin.site.register(TermsImg, TermsImgAdmin)
admin.site.register(Testimonials)
admin.site.register(TestimonialsImg, TestimonialsImgAdmin)
