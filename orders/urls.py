from django.urls import path
from .views import *

app_name = 'orders'

urlpatterns = [
    path('cart/', cart_view, name='cart'),
    path('checkout/', checkout_view, name='checkout'),
    path('update_item/', updateItem, name='update_item'),

]
