from django import forms
from .models import UserRegistration


class UserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', required=True)
    last_name = forms.CharField(label='Last Name', required=True)

    class Meta:
        model = UserRegistration
        fields = (
            'first_name',
            'last_name',
            'phone_no',
            'email',
            'country',
            'county',
            'city',
            'street',
            'zip_code',
        )
